let app = new function() {

  let deletingID;
  let updatingID;

  let ascendingName      = false;
  let ascendingPrice     = true;
  let lastSelectedFilter = 'name';

  let getMaterialCode  = document.getElementById('add-material-code');
  let getName = document.getElementById('add-name');
  let getType = document.getElementById('add-type');
  let getStock = document.getElementById('add-stock');
  let getUOM = document.getElementById('add-uom');

  let editMaterialCode  = document.getElementById('edit-material-code');
  let editName = document.getElementById('edit-name');
  let editType = document.getElementById('edit-type');
  let editStock = document.getElementById('edit-stock');
  let editUOM = document.getElementById('edit-uom');

  let incomingQuantity = document.getElementById('incoming-quantity');
  let outgoingQuantity = document.getElementById('outgoing-quantity');

  let addModule    = document.getElementById('add-module');
  let editModule   = document.getElementById('edit-module');
  let deleteModule = document.getElementById('delete-module');
  let inactiveMask = document.getElementById('inactive-mask');
  let infoModule = document.getElementById('info-module');
  let incomingModule = document.getElementById('incoming-module');
  let outgoingModule = document.getElementById('outgoing-module');

  let addErrorMaterialField  = document.getElementById('add-error-information-material');
  let addErrorNameField = document.getElementById('add-error-information-name');
  let addErrorTypeField = document.getElementById('add-error-information-type');
  let addErrorStockField = document.getElementById('add-error-information-stock');
  let addErrorUOMField = document.getElementById('add-error-information-uom');

  let editErrorMaterialField  = document.getElementById('edit-error-information-material');
  let editErrorNameField = document.getElementById('edit-error-information-name');
  let editErrorTypeField = document.getElementById('edit-error-information-type');
  let editErrorStockField = document.getElementById('edit-error-information-stock');
  let editErrorUOMField = document.getElementById('edit-error-information-uom');

  this.table    = document.getElementById('products');
  var host = 'https://optimized.kingwijati.com/'

  this.FetchAll = function() {
    fetch(host+'api/product/list.abs')
    .then(response => response.json())
    .then(function(data) {
      data.data.sort(function(a, b) {
        return (a.code.localeCompare(b.code));
      });
      console.log(data);

      let result = ''
      data.data.forEach((product) => {
        result += '<tr>';
        result += '<td class="app__table-name-value">' + product.code +
                  '</td>';
        result += '<td>' + product.name
                + '</td>';
        result += '<td>' + product.materialType
                + '</td>';
        result += '<td>' + product.stock
                + '</td>';
        result += '<td>' + product.uom
                + '</td>';
        result += '<td><div class="app__table-actions-block">' +
                '<button onclick="app.Edit(' + product.id + ')">Edit</button>' +
                '<button onclick="app.Incoming(' + product.id + ')">Incoming</button>' +
                '<button onclick="app.Outgoing(' + product.id + ')">Outgoing</button>'+
                '<button onclick="app.Delete(' + product.id + ')">Delete</button></div></td>';
        result += '</tr>';
      });
      document.getElementById('products').innerHTML = result;      
    });
  };

  this.Add = function() {
    
    let materialCode = getMaterialCode.value.trim();
    let name = getName.value;
    let type = getType.value.trim();
    let stock = getStock.value;
    let uom = getUOM.value.trim();

    if (this.Check('get')) {
        addModule.style.display = 'none';
        inactiveMask.style.display = 'none';

        fetch(host+'api/product/save.abs?code='+materialCode+'&name='+name+'&materialType='+type+'&stock='+stock+'&uom='+uom)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
        })
        .then(() => {
          this.FetchAll();
          getMaterialCode.value  = '';
          getName.value = '';
          getType.value = '';
          getStock.value = '';
          getUOM.value = '';
        });
      }
  };

  this.AddNew = function() {
    addModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    getMaterialCode.value  = '';
    getName.value = '';
    getType.value = '';
    getStock.value = '';
    getUOM.value = '';

    getMaterialCode.style.borderColor  = '#0C2427';
    getName.style.borderColor = '#0C2427';
    getType.style.borderColor = '#0C2427';
    getStock.style.borderColor = '#0C2427';
    getUOM.style.borderColor = '#0C2427';

    addErrorMaterialField.innerHTML = '';
    addErrorNameField.innerHTML = '';
    addErrorTypeField.innerHTML = '';
    addErrorStockField.innerHTML = '';
    addErrorUOMField.innerHTML = '';
  };

  this.closeAddForm = function() {
    addModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.Delete = function(item) {
    deletingID = item;
    deleteModule.style.display = 'flex';
    inactiveMask.style.display = 'block';
  };

  this.closeDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.applyDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    fetch(host+'api/product/delete.abs?id='+deletingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
    })
    .then(() => {
      this.FetchAll();
    });
  };

  this.Edit = function(item) {
    editModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    updatingID = item;

    editMaterialCode.style.borderColor = '#0C2427';
    editName.style.borderColor = '#0C2427';
    editType.style.borderColor = '#0C2427';
    editStock.style.borderColor = '#0C2427';
    editUOM.style.borderColor = '#0C2427';

    fetch(host+'api/product/detail.abs?id='+updatingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data.data);
      editMaterialCode.value = data.data.code;
      editName.value = data.data.name;
      editStock.value = data.data.stock;
      editType.value = data.data.materialType;
      editUOM.value = data.data.uom;

      editErrorMaterialField.innerHTML = '';
      editErrorNameField.innerHTML = '';
      editErrorStockField.innerHTML = '';
      editErrorTypeField.innerHTML = '';
      editErrorUOMField.innerHTML = '';
    });
  };

  this.closeEditForm = function() {
   editModule.style.display = 'none';
   inactiveMask.style.display = 'none';
  }

  this.Incoming = function(item) {
    updatingID = item;
    incomingQuantity.value = 1;
    incomingModule.style.display = 'flex';
    inactiveMask.style.display = 'block';
  };

  this.applyIncomingForm = function() {
    incomingModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    let quantity = incomingQuantity.value;
    fetch(host+'api/product/incoming.abs?id='+updatingID+'&quantity='+quantity)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
      infoModule.style.display = 'flex';
      inactiveMask.style.display = 'block';
      document.getElementById('info-text-module').innerHTML = data.data.description;
    });
  };

  this.closeIncomingForm = function() {
    incomingModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    this.FetchAll();
  }

  this.Outgoing = function(item) {
    updatingID = item;
    outgoingQuantity.value = 1;
    outgoingModule.style.display = 'flex';
    inactiveMask.style.display = 'block';
  };

  this.applyOutgoingForm = function() {
    outgoingModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    let quantity = outgoingQuantity.value;
    fetch(host+'api/product/outgoing.abs?id='+updatingID+'&quantity='+quantity)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
      infoModule.style.display = 'flex';
      inactiveMask.style.display = 'block';
      document.getElementById('info-text-module').innerHTML = data.data.description;
    });
  };

  this.closeOutgoingForm = function() {
    outgoingModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    this.FetchAll();
  }

  this.closeInfoForm = function() {
    infoModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    this.FetchAll();
  }

   this.applyEditForm = function() {
     if (this.Check('edit')) {

      let materialCode = editMaterialCode.value;
      let name = editName.value;
      let type = editType.value;
      let stock = editStock.value;
      let uom = editUOM.value;

      fetch(host+'api/product/update.abs?id='+updatingID+'&code='+materialCode+'&name='+name+'&materialType='+type+'&stock='+stock+'&uom='+uom)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
        })
        .then(() => {
          this.FetchAll();
          editModule.style.display = 'none';
          inactiveMask.style.display = 'none';
        });       
     }
   };

//   this.Sort = function(param) {
//     if (param === 'name') {
//       if (ascendingName) {
//         this.products.sort(compareDescendingName);
//         ascendingName = false;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingName);
//         ascendingName = true;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9650;"
//       }
//     };

//     if (param === 'price') {
//       if (ascendingPrice) {
//         this.products.sort(compareDescendingPrice);
//         ascendingPrice = false;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingPrice);
//         ascendingPrice = true;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9650;"
//       }
//     }

//     this.FetchAll();

//     function compareAscendingPrice(x, y) {
//       return x.price - y.price;
//     }
//     function compareDescendingPrice(x, y) {
//       return y.price - x.price;
//     }

//     function compareAscendingName(x, y) {
//       if (x.name > y.name) return 1;
//       if (x.name < y.name) return -1;
//     }
//     function compareDescendingName(x, y) {
//       if (x.name > y.name) return -1;
//       if (x.name < y.name) return 1;
//     }
//   };

//   // сортировка элементов таблицы по параметру name при загрузке страницы
//   this.Sort('name');

//   this.Search = function() {
//     let searchValue = document.getElementById('search-form').value.toLowerCase().trim();
//     let searchArray = this.products;
//     let searchCompleted = [];

//     for (i = 0; i < searchArray.length; i++) {
//       let currentStr = searchArray[i].name.slice(0, searchValue.length).toLowerCase();
//       if (searchValue === currentStr) {
//         searchCompleted.push(searchArray[i]);
//       }
//     }

//     this.products = searchCompleted;
//     this.FetchAll();
//     document.getElementById('search-form').value = '';
//   };

//   /*
//    * Проверяет корректность заполнения формы
//    *
//    * @param {get} - проверка формы добавления нового элемента
//    * @param {edit} - проверка формы редактирования элемента
//    * return {true/false} - корректно/некорректно заполнена форма
//    */
  this.Check = function(param) {
    let checker = true;

    if (param === 'get') {
      if (getMaterialCode.value.trim() === '') {
        getMaterialCode.style.borderColor = '#F2223C';
        addErrorMaterialField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getName.value.trim() === '') {
        getName.style.borderColor = '#F2223C';
        addErrorNameField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getType.value.trim() === '') {
        getType.style.borderColor = '#F2223C';
        addErrorTypeField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getStock.value.trim() === '') {
        getStock.style.borderColor = '#F2223C';
        addErrorStockField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getUOM.value.trim() === '') {
        getUOM.style.borderColor = '#F2223C';
        addErrorUOMField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }

    if (param === 'edit') {
      if (editMaterialCode.value.trim() === '') {
        editMaterialCode.style.borderColor = '#F2223C';
        editErrorMaterialField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (editName.value.trim() === '') {
        editName.style.borderColor = '#F2223C';
        editErrorNameField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (editType.value.trim() === '') {
        editType.style.borderColor = '#F2223C';
        editErrorTypeField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (editStock.value.trim() === '') {
        editStock.style.borderColor = '#F2223C';
        editErrorStockField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (editUOM.value.trim() === '') {
        editUOM.style.borderColor = '#F2223C';
        editErrorUOMField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }
  };

//   // подсвечивание некорректных значений после потери фокуса

//   getName.onblur = function() {
//     if (getName.value.trim() === '') {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (getName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                     ' exceed 15 characters.';
//     }
//   }
//   getName.onfocus = function() {
//       getName.style.borderColor = '#0C2427';
//       addErrorNameField.innerHTML = '';
//   };

//   getCount.onblur = function() {
//     if (getCount.value.trim() === '') {
//       getCount.style.borderColor = '#F2223C';
//       addErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getCount.onfocus = function() {
//       getCount.style.borderColor = '#0C2427';
//       addErrorCountField.innerHTML = '';
//   };

//   getPrice.onblur = function() {
//     if (getPrice.value.trim() === '') {
//       getPrice.style.borderColor = '#F2223C';
//       addErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getPrice.onfocus = function() {
//       getPrice.style.borderColor = '#0C2427';
//       addErrorPriceField.innerHTML = '';
//   };

//   editName.onblur = function() {
//     if (editName.value.trim() === '') {
//       editName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (editName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                      ' exceed 15 characters.';
//     }
//   }
//   editName.onfocus = function() {
//       editName.style.borderColor = '#0C2427';
//       editErrorNameField.innerHTML = '';
//   };

//   editCount.onblur = function() {
//     if (editCount.value.trim() === '') {
//       editCount.style.borderColor = '#F2223C';
//       editErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editCount.onfocus = function() {
//       editCount.style.borderColor = '#0C2427';
//       editErrorCountField.innerHTML = '';
//   };

//   editPrice.onblur = function() {
//     if (editPrice.value.trim() === '') {
//       editPrice.style.borderColor = '#F2223C';
//       editErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editPrice.onfocus = function() {
//       editPrice.style.borderColor = '#0C2427';
//       editErrorPriceField.innerHTML = '';
//   };

};
app.FetchAll();

