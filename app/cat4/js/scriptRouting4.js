let app = new function() {

  let deletingID;
  let updatingID;
  let producingID;
  let qcID;

  let ascendingName      = false;
  let ascendingPrice     = true;
  let lastSelectedFilter = 'name';
  let statusProcessId = '';

  let getCWCCode  = document.getElementById('add-cwc-code');
  let getNWCCode  = document.getElementById('add-nwc-code');
  let getCMCode  = document.getElementById('add-cm-code');
  let getNMCode  = document.getElementById('add-nm-code');
  let getIsFirst = document.getElementById('add-is-first');
  let getIsLast = document.getElementById('add-is-last');

  let editQuantity = document.getElementById('edit-quantity');

  let addModule    = document.getElementById('add-module');
  let editModule   = document.getElementById('edit-module');
  let deleteModule = document.getElementById('delete-module');
  let produceModule = document.getElementById('produce-module');
  let infoModule = document.getElementById('info-module');
  let qcModule = document.getElementById('qc-module');
  let inactiveMask = document.getElementById('inactive-mask');
  let inactiveMask1 = document.getElementById('inactive-mask1');

  let rejectMaterialQuantity = document.getElementById('reject-quantity');

  let addErrorCWCField  = document.getElementById('add-error-information-cwc');
  let addErrorNWCField  = document.getElementById('add-error-information-nwc');
  let addErrorCMField  = document.getElementById('add-error-information-cm');
  let addErrorNMField  = document.getElementById('add-error-information-nm');
  let addErrorIsFirstField  = document.getElementById('add-error-information-is-first');
  let addErrorIsLastField  = document.getElementById('add-error-information-is-last');
  
  let editErrorQuantityField = document.getElementById('edit-error-information-quantity');

  let rejectErrorField = document.getElementById('reject-error-information');


  this.table    = document.getElementById('products');
  var host = 'https://optimized.kingwijati.com/'

  this.FetchAll = function() {
    if (statusProcessId !== '') {
      document.getElementById('front-app').style.display = "none";
      document.getElementById('status-app').style.display = "";
      document.getElementById('header-id').innerHTML = "Routing for: " + statusProcessId;
      fetch(host+'api/routing/list.abs')
      .then(response => response.json())
      .then(function(data) {
        data.data.sort(function(a, b) {
          return (a.id - b.id);
        });
        console.log(data);

        let resultStatus = '';
        data.data.forEach((product) => {
          if (product.finishedGoodCode === statusProcessId) {
            resultStatus += '<tr>';
            resultStatus += '<td class="app__table-name-value">' + product.currentWorkCentreCode +
                      '</td>';
            resultStatus += '<td class="app__table-name-value">' + product.nextWorkCentreCode +
                      '</td>';
            resultStatus += '<td>' + product.currentMaterialCode
                    + '</td>';
            resultStatus += '<td>' + product.nextMaterialCode
                    + '</td>';
            if (product.isFirst === 'true') {
              resultStatus += '<td>' + 'First'
                    + '</td>';
            } else if (product.isLast === 'true') {
              resultStatus += '<td>' + 'Last'
                    + '</td>';
            } else {
              resultStatus += '<td>' + '-'
                    + '</td>';
            }
            resultStatus += '<td><div class="app__table-actions-block">' +
                    '<button onclick="app.Delete(\'' + product.id + '\')">Delete</button></td>';
            resultStatus += '</tr>';
          }
        });
        document.getElementById('statusTable').innerHTML = resultStatus;
      });
    } else {
      document.getElementById('status-app').style.display = "none";
      document.getElementById('front-app').style.display = "";

      fetch(host+'api/product/list.abs')
      .then(response => response.json())
      .then(function(data) {
        data.data.sort(function(a, b) {
          return (a.code.localeCompare(b.code));
        });
        console.log(data);

        let processIdArr = [];

        data.data.forEach((product) => {
          if (!arrayContains(processIdArr, product.code) && product.materialType === "FG") {
            processIdArr.push(product.code);
          }
        })

        let resultCreated = ''
        for (var i = 0; i < processIdArr.length; i++) {
          resultCreated += '<tr>';
          resultCreated += '<td class="app__table-name-value">' + processIdArr[i] +
                    '</td>';
          resultCreated += '<td><div class="app__table-actions-block">' +
                  '<button onclick="app.StatusProcess(\'' + processIdArr[i] + '\')">Status</button>';
          resultCreated += '</tr>';
        }

        document.getElementById('frontTable').innerHTML = resultCreated;   
      });
    }    
  };

  this.Add = function() {
    
    let currentWorkCentreCode = getCWCCode.value;
    let nextWorkCentreCode = getNWCCode.value;
    let currentMaterialCode = getCMCode.value;
    let nextMaterialCode = getNMCode.value;

    if (this.Check('get')) {
        addModule.style.display = 'none';
        inactiveMask1.style.display = 'none';

        fetch(host+'api/routing/save.abs?currentworkcentrecode='+currentWorkCentreCode+'&nextworkcentrecode='+nextWorkCentreCode+'&finishedgoodcode='+statusProcessId+'&currentmaterialcode='+currentMaterialCode+'&nextmaterialcode='+nextMaterialCode+'&isfirst='+getIsFirst.checked+'&islast='+getIsLast.checked)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
        })
        .then(() => {
          this.FetchAll();
          getCWCCode.value = '';
          getNWCCode.value = '';
          getCMCode.value = '';
          getNMCode.value = '';
          getIsFirst.checked = false;
          getIsLast.checked = false;
        });
      }
  };

  this.AddNew = function() {
    addModule.style.display = 'flex';
    inactiveMask1.style.display = 'block';

    getIsFirst.checked = false;
    getIsLast.checked = false;

    addErrorCWCField.innerHTML  = '';
    addErrorNWCField.innerHTML  = '';
    addErrorCMField.innerHTML  = '';
    addErrorNMField.innerHTML  = '';
    addErrorIsFirstField.innerHTML  = '';
    addErrorIsLastField.innerHTML = '';

    Promise.all([
      fetch(host+'api/billOfMaterial/list.abs?'),
      fetch(host+'api/workcentre/list.abs?')
    ])
    .then(result => Promise.all(result.map(v => v.json()))
    .then(function(data) {
      console.log(data[0]);
      console.log(data[1]);

      let materialData = '';
      data[0].data.forEach((material) => {
        if (material.materialCode === statusProcessId) {
          materialData += '<option value="'+material.subMaterialCode+'">'+material.subMaterialCode+'</option>';
        } 
      });
      document.getElementById('add-cm-code').innerHTML = materialData;
      materialData += '<option value="'+statusProcessId+'">'+statusProcessId+'</option>'
      document.getElementById('add-nm-code').innerHTML = materialData;

      let workCentreData = '';
      data[1].data.forEach((workCentre) => {
        workCentreData += '<option value="'+workCentre.code+'">'+workCentre.code+'</option>';
      });
      document.getElementById('add-cwc-code').innerHTML = workCentreData;
      document.getElementById('add-nwc-code').innerHTML = workCentreData;

      getCWCCode.style.borderColor  = '#0C2427';
      getNWCCode.style.borderColor  = '#0C2427';
      getCMCode.style.borderColor  = '#0C2427';
      getNMCode.style.borderColor  = '#0C2427';
      getIsFirst.style.borderColor  = '#0C2427';
      getIsLast.style.borderColor  = '#0C2427';
    }));
  };

  this.closeAddForm = function() {
    addModule.style.display = 'none';
    inactiveMask1.style.display = 'none';
  };

  this.StatusProcess = function(id) {
    statusProcessId = id;
    this.FetchAll();
  };

  this.Back = function() {
    statusProcessId = '';
    this.FetchAll();
  };

  this.Delete = function(item) {
    deletingID = item;
    deleteModule.style.display = 'flex';
    inactiveMask1.style.display = 'block';
  };

  this.closeDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask1.style.display = 'none';
  };

  this.applyDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask1.style.display = 'none';
    fetch(host+'api/routing/delete.abs?id='+deletingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
    })
    .then(() => {
      this.FetchAll();
    });
  };

  this.Produce = function(item) {
    producingID = item;
    produceModule.style.display = 'flex';
    inactiveMask1.style.display = 'block';
  };

  this.closeProduceForm = function() {
    produceModule.style.display = 'none';
    inactiveMask1.style.display = 'none';
  };

  this.applyProduceForm = function() {
    produceModule.style.display = 'none';
    inactiveMask1.style.display = 'none';
    fetch(host+'api/materialProcess/production.abs?id='+producingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
      infoModule.style.display = 'flex';
      inactiveMask1.style.display = 'block';
      document.getElementById('info-text-module').innerHTML = data.data.description;
    });
  };

  this.closeInfoForm = function() {
    infoModule.style.display = 'none';
    inactiveMask1.style.display = 'none';
    this.FetchAll();
  }

  this.Edit = function(item) {
    editModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    updatingID = item;

    editQuantity.style.borderColor = '#0C2427';

    fetch(host+'api/materialProcess/detail.abs?id='+updatingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data.data);
      editQuantity.value = data.data.quantity;

      editErrorQuantityField.innerHTML = '';
    });
  };

  this.closeEditForm = function() {
   editModule.style.display = 'none';
   inactiveMask.style.display = 'none';
  }

  this.applyEditForm = function() {
    if (this.Check('edit')) {
      fetch(host+'api/materialProcess/detail.abs?id='+updatingID)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
          let code = data.data.code;
          let workCentreCode = data.data.workCentreCode;
          let statusProcess = data.data.statusProcess;
          let quantity = editQuantity.value;
          let datestamp = data.data.datestamp;

          fetch(host+'api/materialProcess/update.abs?id='+updatingID+'&code='+code+'&workcentrecode='+workCentreCode+'&status='+statusProcess+'&quantity='+quantity+'&datestamp='+datestamp)
          .then(response => response.json())
          .then(function(data) {
            console.log(data);
            window.location.reload(true);
          });
        });
     }
   };

  this.QC = function(item) {
    qcID = item;
    qcModule.style.display = 'flex';
    inactiveMask1.style.display = 'block';

    rejectErrorField.innerHTML = '';
  };

  this.applyQCForm = function() {
    fetch(host+'api/materialProcess/detail.abs?id='+qcID)
      .then(response => response.json())
      .then(function(data) {
        let checker = true;
        console.log(data);
        if (rejectMaterialQuantity.value > data.data.quantity) {
          rejectMaterialQuantity.style.borderColor = '#F2223C';
          rejectErrorField.innerHTML = 'The rejected amount is higher than the processed amount';
          checker = false;
        }
        if (rejectMaterialQuantity.value === '') {
          rejectMaterialQuantity.style.borderColor = '#F2223C';
          rejectErrorField.innerHTML = 'Please, enter the value.';
          checker = false;
        }
        return checker;
      }).then(function(checker) {
        console.log(checker);
        if(checker) {
          let quantity = rejectMaterialQuantity.value;

          fetch(host+'api/materialProcess/qc.abs?id='+qcID+'&reject='+quantity)
          .then(response => response.json())
          .then(function(data) {
            console.log(data);
          }).then(() => {
            qcModule.style.display = 'none';
            inactiveMask.style.display = 'none';
            window.location.reload(true);
          });            
        }
      });
  }

  this.closeQCForm = function() {
    qcModule.style.display = 'none';
    inactiveMask1.style.display = 'none';
   }

//   this.Sort = function(param) {
//     if (param === 'name') {
//       if (ascendingName) {
//         this.products.sort(compareDescendingName);
//         ascendingName = false;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingName);
//         ascendingName = true;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9650;"
//       }
//     };

//     if (param === 'price') {
//       if (ascendingPrice) {
//         this.products.sort(compareDescendingPrice);
//         ascendingPrice = false;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingPrice);
//         ascendingPrice = true;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9650;"
//       }
//     }

//     this.FetchAll();

//     function compareAscendingPrice(x, y) {
//       return x.price - y.price;
//     }
//     function compareDescendingPrice(x, y) {
//       return y.price - x.price;
//     }

//     function compareAscendingName(x, y) {
//       if (x.name > y.name) return 1;
//       if (x.name < y.name) return -1;
//     }
//     function compareDescendingName(x, y) {
//       if (x.name > y.name) return -1;
//       if (x.name < y.name) return 1;
//     }
//   };

//   // сортировка элементов таблицы по параметру name при загрузке страницы
//   this.Sort('name');

//   this.Search = function() {
//     let searchValue = document.getElementById('search-form').value.toLowerCase().trim();
//     let searchArray = this.products;
//     let searchCompleted = [];

//     for (i = 0; i < searchArray.length; i++) {
//       let currentStr = searchArray[i].name.slice(0, searchValue.length).toLowerCase();
//       if (searchValue === currentStr) {
//         searchCompleted.push(searchArray[i]);
//       }
//     }

//     this.products = searchCompleted;
//     this.FetchAll();
//     document.getElementById('search-form').value = '';
//   };

//   /*
//    * Проверяет корректность заполнения формы
//    *
//    * @param {get} - проверка формы добавления нового элемента
//    * @param {edit} - проверка формы редактирования элемента
//    * return {true/false} - корректно/некорректно заполнена форма
//    */
  this.Check = function(param) {
    let checker = true;

    if (param === 'get') {
      if (getCWCCode.value === '' && getNWCCode.value === '') {
        getCWCCode.style.borderColor = '#F2223C';
        addErrorCWCField.innerHTML = 'Please, enter at least one value.';
        getNWCCode.style.borderColor = '#F2223C';
        addErrorNWCField.innerHTML = 'Please, enter at least one value.';
        checker = false;
      }
      if (getCMCode.value === '' && getNMCode.value === '') {
        getCMCode.style.borderColor = '#F2223C';
        addErrorCMField.innerHTML = 'Please, enter at least one value.';
        getNMCode.style.borderColor = '#F2223C';
        addErrorNMField.innerHTML = 'Please, enter at least one value.';
        checker = false;
      }
      if (getIsFirst.checked && getIsLast.checked) {
        getIsFirst.style.borderColor = '#F2223C';
        addErrorIsFirstField.innerHTML = 'You can not check these 2 boxes at the same time.';
        getIsLast.style.borderColor = '#F2223C';
        addErrorIsLastField.innerHTML = 'You can not check these 2 boxes at the same time.';
        checker = false;
      }
      if (getIsLast.checked && getNMCode.value !== statusProcessId) {
        getNMCode.style.borderColor = '#F2223C';
        addErrorNMField.innerHTML = 'Material need to be finished good if last work centre.';
        getIsLast.style.borderColor = '#F2223C';
        addErrorIsLastField.innerHTML = 'Material need to be finished good if last work centre.';
        checker = false;
      }
      if (getCWCCode.value === getNWCCode.value) {
        getCWCCode.style.borderColor = '#F2223C';
        addErrorCWCField.innerHTML = 'Work centre can not be the same.';
        getNWCCode.style.borderColor = '#F2223C';
        addErrorNWCField.innerHTML = 'Work centre can not be the same.';
        checker = false;
      }
      if (getCMCode.value === getNMCode.value) {
        getCMCode.style.borderColor = '#F2223C';
        addErrorCMField.innerHTML = 'Material can not be the same.';
        getNMCode.style.borderColor = '#F2223C';
        addErrorNMField.innerHTML = 'Material can not be the same.';
        checker = false;
      }
      return checker;
    }

    if (param === 'edit') {
      if (editQuantity.value === '') {
        editQuantity.style.borderColor = '#F2223C';
        editErrorQuantityField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }
  };

  function arrayContains(arr, searchFor){
    if(typeof arr.includes == 'undefined'){
        var arrLength = arr.length;
        for (var i = 0; i < arrLength; i++) {
            if (arr[i] === searchFor) {
                return true;
            }
        }
        return false;
    }
    return arr.includes(searchFor);
}

//   // подсвечивание некорректных значений после потери фокуса

//   getName.onblur = function() {
//     if (getName.value.trim() === '') {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (getName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                     ' exceed 15 characters.';
//     }
//   }
//   getName.onfocus = function() {
//       getName.style.borderColor = '#0C2427';
//       addErrorNameField.innerHTML = '';
//   };

//   getCount.onblur = function() {
//     if (getCount.value.trim() === '') {
//       getCount.style.borderColor = '#F2223C';
//       addErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getCount.onfocus = function() {
//       getCount.style.borderColor = '#0C2427';
//       addErrorCountField.innerHTML = '';
//   };

//   getPrice.onblur = function() {
//     if (getPrice.value.trim() === '') {
//       getPrice.style.borderColor = '#F2223C';
//       addErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getPrice.onfocus = function() {
//       getPrice.style.borderColor = '#0C2427';
//       addErrorPriceField.innerHTML = '';
//   };

//   editName.onblur = function() {
//     if (editName.value.trim() === '') {
//       editName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (editName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                      ' exceed 15 characters.';
//     }
//   }
//   editName.onfocus = function() {
//       editName.style.borderColor = '#0C2427';
//       editErrorNameField.innerHTML = '';
//   };

//   editCount.onblur = function() {
//     if (editCount.value.trim() === '') {
//       editCount.style.borderColor = '#F2223C';
//       editErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editCount.onfocus = function() {
//       editCount.style.borderColor = '#0C2427';
//       editErrorCountField.innerHTML = '';
//   };

//   editPrice.onblur = function() {
//     if (editPrice.value.trim() === '') {
//       editPrice.style.borderColor = '#F2223C';
//       editErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editPrice.onfocus = function() {
//       editPrice.style.borderColor = '#0C2427';
//       editErrorPriceField.innerHTML = '';
//   };

};
app.FetchAll();

