let app = new function() {

  let deletingID;
  let updatingID;

  let ascendingName      = false;
  let ascendingPrice     = true;
  let lastSelectedFilter = 'name';

  let getWorkCentreCode  = document.getElementById('add-work-centre-code');
  let getName = document.getElementById('add-name');
  let getDurations = document.getElementById('add-durations');

  let editWorkCentreCode  = document.getElementById('edit-work-centre-code');
  let editName = document.getElementById('edit-name');
  let editDurations = document.getElementById('edit-durations');

  let addModule    = document.getElementById('add-module');
  let editModule   = document.getElementById('edit-module');
  let deleteModule = document.getElementById('delete-module');
  let inactiveMask = document.getElementById('inactive-mask');

  let addErrorWorkCentreCodeField  = document.getElementById('add-error-information-work-centre');
  let addErrorNameField = document.getElementById('add-error-information-name');
  let addErrorDurationsField = document.getElementById('add-error-information-durations');

  let editErrorWorkCentreCodeField  = document.getElementById('edit-error-information-work-centre');
  let editErrorNameField = document.getElementById('edit-error-information-name');
  let editErrorDurationsField = document.getElementById('edit-error-information-durations');


  this.table    = document.getElementById('products');
  var host = 'https://managed.kingwijati.com/'

  this.FetchAll = function() {
    fetch(host+'api/workcentre/list.abs')
    .then(response => response.json())
    .then(function(data) {
      data.data.sort(function(a, b) {
        return (a.code.localeCompare(b.code));
      });
      console.log(data);

      let result = ''
      data.data.forEach((product) => {
        result += '<tr>';
        result += '<td class="app__table-name-value">' + product.code +
                  '</td>';
        result += '<td>' + product.name
                + '</td>';
        result += '<td>' + product.durations
                + '</td>';
        result += '<td><div class="app__table-actions-block">' +
                '<button onclick="app.Edit(' + product.id + ')">Edit</button>' +
                '<button onclick="app.Delete(' + product.id + ')">Delete</button></div></td>';
        result += '</tr>';
      });
      document.getElementById('products').innerHTML = result;      
    });
  };

  this.Add = function() {
    
    let workCentreCode = getWorkCentreCode.value.trim();
    let name = getName.value.trim();
    let durations = getDurations.value;

    if (this.Check('get')) {
        addModule.style.display = 'none';
        inactiveMask.style.display = 'none';

        fetch(host+'api/workcentre/save.abs?code='+workCentreCode+'&name='+name+'&durations='+durations)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
        })
        .then(() => {
          this.FetchAll();
          getWorkCentreCode.value  = '';
          getName.value = '';
          getDurations.value = '';
        });
      }
  };

  this.AddNew = function() {
    addModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    getWorkCentreCode.value  = '';
    getName.value = '';
    getDurations.value = '';

    getWorkCentreCode.style.borderColor  = '#0C2427';
    getName.style.borderColor = '#0C2427';
    getDurations.style.borderColor = '#0C2427';

    addErrorWorkCentreCodeField.innerHTML = '';
    addErrorNameField.innerHTML = '';
    addErrorDurationsField.innerHTML = '';
  };

  this.closeAddForm = function() {
    addModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.Delete = function(item) {
    deletingID = item;
    deleteModule.style.display = 'flex';
    inactiveMask.style.display = 'block';
  };

  this.closeDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.applyDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    fetch(host+'api/workcentre/delete.abs?id='+deletingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
    })
    .then(() => {
      this.FetchAll();
    });
  };

  this.Edit = function(item) {
    editModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    updatingID = item;

    editWorkCentreCode.style.borderColor = '#0C2427';
    editName.style.borderColor = '#0C2427';
    editDurations.style.borderColor = '#0C2427';

    fetch(host+'api/workcentre/detail.abs?id='+updatingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data.data);
      editWorkCentreCode.value = data.data.code;
      editName.value = data.data.name;
      editDurations.value = data.data.durations;

      editErrorWorkCentreCodeField.innerHTML = '';
      editErrorNameField.innerHTML = '';
      editErrorDurationsField.innerHTML = '';
    });
  };

  this.closeEditForm = function() {
   editModule.style.display = 'none';
   inactiveMask.style.display = 'none';
  }

   this.applyEditForm = function() {
     if (this.Check('edit')) {

      let workCentreCode = editWorkCentreCode.value;
      let name = editName.value;
      let durations = editDurations.value;

      fetch(host+'api/workcentre/update.abs?id='+updatingID+'&code='+workCentreCode+'&name='+name+'&durations='+durations)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
        })
        .then(() => {
          this.FetchAll();
          editModule.style.display = 'none';
          inactiveMask.style.display = 'none';
        });       
     }
   };

//   this.Sort = function(param) {
//     if (param === 'name') {
//       if (ascendingName) {
//         this.products.sort(compareDescendingName);
//         ascendingName = false;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingName);
//         ascendingName = true;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9650;"
//       }
//     };

//     if (param === 'price') {
//       if (ascendingPrice) {
//         this.products.sort(compareDescendingPrice);
//         ascendingPrice = false;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingPrice);
//         ascendingPrice = true;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9650;"
//       }
//     }

//     this.FetchAll();

//     function compareAscendingPrice(x, y) {
//       return x.price - y.price;
//     }
//     function compareDescendingPrice(x, y) {
//       return y.price - x.price;
//     }

//     function compareAscendingName(x, y) {
//       if (x.name > y.name) return 1;
//       if (x.name < y.name) return -1;
//     }
//     function compareDescendingName(x, y) {
//       if (x.name > y.name) return -1;
//       if (x.name < y.name) return 1;
//     }
//   };

//   // сортировка элементов таблицы по параметру name при загрузке страницы
//   this.Sort('name');

//   this.Search = function() {
//     let searchValue = document.getElementById('search-form').value.toLowerCase().trim();
//     let searchArray = this.products;
//     let searchCompleted = [];

//     for (i = 0; i < searchArray.length; i++) {
//       let currentStr = searchArray[i].name.slice(0, searchValue.length).toLowerCase();
//       if (searchValue === currentStr) {
//         searchCompleted.push(searchArray[i]);
//       }
//     }

//     this.products = searchCompleted;
//     this.FetchAll();
//     document.getElementById('search-form').value = '';
//   };

//   /*
//    * Проверяет корректность заполнения формы
//    *
//    * @param {get} - проверка формы добавления нового элемента
//    * @param {edit} - проверка формы редактирования элемента
//    * return {true/false} - корректно/некорректно заполнена форма
//    */
  this.Check = function(param) {
    let checker = true;

    if (param === 'get') {
      if (getWorkCentreCode.value.trim() === '') {
        getWorkCentreCode.style.borderColor = '#F2223C';
        addErrorWorkCentreCodeField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getName.value.trim() === '') {
        getName.style.borderColor = '#F2223C';
        addErrorNameField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getDurations.value.trim() === '') {
        getDurations.style.borderColor = '#F2223C';
        addErrorDurationsField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }

    if (param === 'edit') {
      if (editWorkCentreCode.value.trim() === '') {
        editWorkCentreCode.style.borderColor = '#F2223C';
        editErrorWorkCentreCodeField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (editName.value.trim() === '') {
        editName.style.borderColor = '#F2223C';
        editErrorNameField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (editDurations.value.trim() === '') {
        editDurations.style.borderColor = '#F2223C';
        editErrorDurationsField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }
  };

//   // подсвечивание некорректных значений после потери фокуса

//   getName.onblur = function() {
//     if (getName.value.trim() === '') {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (getName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                     ' exceed 15 characters.';
//     }
//   }
//   getName.onfocus = function() {
//       getName.style.borderColor = '#0C2427';
//       addErrorNameField.innerHTML = '';
//   };

//   getCount.onblur = function() {
//     if (getCount.value.trim() === '') {
//       getCount.style.borderColor = '#F2223C';
//       addErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getCount.onfocus = function() {
//       getCount.style.borderColor = '#0C2427';
//       addErrorCountField.innerHTML = '';
//   };

//   getPrice.onblur = function() {
//     if (getPrice.value.trim() === '') {
//       getPrice.style.borderColor = '#F2223C';
//       addErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getPrice.onfocus = function() {
//       getPrice.style.borderColor = '#0C2427';
//       addErrorPriceField.innerHTML = '';
//   };

//   editName.onblur = function() {
//     if (editName.value.trim() === '') {
//       editName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (editName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                      ' exceed 15 characters.';
//     }
//   }
//   editName.onfocus = function() {
//       editName.style.borderColor = '#0C2427';
//       editErrorNameField.innerHTML = '';
//   };

//   editCount.onblur = function() {
//     if (editCount.value.trim() === '') {
//       editCount.style.borderColor = '#F2223C';
//       editErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editCount.onfocus = function() {
//       editCount.style.borderColor = '#0C2427';
//       editErrorCountField.innerHTML = '';
//   };

//   editPrice.onblur = function() {
//     if (editPrice.value.trim() === '') {
//       editPrice.style.borderColor = '#F2223C';
//       editErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editPrice.onfocus = function() {
//       editPrice.style.borderColor = '#0C2427';
//       editErrorPriceField.innerHTML = '';
//   };

};
app.FetchAll();

