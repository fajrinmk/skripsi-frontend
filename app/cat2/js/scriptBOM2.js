let app = new function() {

  let deletingID;
  let updatingID;

  let ascendingName      = false;
  let ascendingPrice     = true;
  let lastSelectedFilter = 'name';

  let getMaterialCode  = document.getElementById('add-material-code');
  let getSubMaterialCode = document.getElementById('add-sub-material-code');
  let getSubQuantity = document.getElementById('add-sub-quantity');

  let editMaterialCode  = document.getElementById('edit-material-code');
  let editSubMaterialCode = document.getElementById('edit-sub-material-code');
  let editQuantity = document.getElementById('edit-quantity');

  let addModule    = document.getElementById('add-module');
  let editModule   = document.getElementById('edit-module');
  let deleteModule = document.getElementById('delete-module');
  let inactiveMask = document.getElementById('inactive-mask');

  let addErrorMaterialField  = document.getElementById('add-error-information-material');
  let addErrorSubMaterialField = document.getElementById('add-error-information-sub-material');
  let addErrorQuantityField = document.getElementById('add-error-information-quantity');

  let editErrorMaterialField  = document.getElementById('edit-error-information-material');
  let editErrorSubMaterialField = document.getElementById('edit-error-information-sub-material');
  let editErrorQuantityField = document.getElementById('edit-error-information-quantity');


  this.table    = document.getElementById('products');
  var host = 'https://standard.kingwijati.com/'

  this.FetchAll = function() {
    fetch(host+'api/billOfMaterial/list.abs')
    .then(response => response.json())
    .then(function(data) {
      data.data.sort(function(a, b) {
        return (a.materialCode.localeCompare(b.materialCode) || a.subMaterialCode.localeCompare(b.subMaterialCode));
      });
      console.log(data);

      let result = ''
      data.data.forEach((product) => {
        result += '<tr>';
        result += '<td class="app__table-name-value">' + product.materialCode +
                  '</td>';
        result += '<td>' + product.subMaterialCode
                + '</td>';
        result += '<td>' + product.subQuantity
                + '</td>';
        result += '<td><div class="app__table-actions-block">' +
                '<button onclick="app.Edit(' + product.id + ')">Edit</button>' +
                '<button onclick="app.Delete(' + product.id + ')">Delete</button></div></td>';
        result += '</tr>';
      });
      document.getElementById('products').innerHTML = result;      
    });
  };

  this.Add = function() {
    
    let materialCode = getMaterialCode.value.trim();
    let subMaterialCode = getSubMaterialCode.value.trim();
    let subQuantity = getSubQuantity.value;

    if (this.Check('get')) {
        addModule.style.display = 'none';
        inactiveMask.style.display = 'none';

        fetch(host+'api/billOfMaterial/save.abs?materialCode='+materialCode+'&subMaterialCode='+subMaterialCode+'&subQuantity='+subQuantity)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
        })
        .then(() => {
          this.FetchAll();
          getMaterialCode.value  = '';
          getSubMaterialCode.value = '';
          getSubQuantity.value = '';
        });
      }
  };

  this.AddNew = function() {
    addModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    getMaterialCode.value  = '';
    getSubMaterialCode.value = '';
    getSubQuantity.value = '';

    fetch(host+'api/product/list.abs?')
    .then(response => response.json())
    .then(function(data) {
      data.data.sort(function(a, b) {
        return (a.code.localeCompare(b.code));
      });
      console.log(data);
      let materialData = '';

      data.data.forEach((material) => {
        materialData += '<option value="'+material.code+'">'+material.code+'</option>';
      });
      document.getElementById('add-material-code').innerHTML = materialData;
      document.getElementById('add-sub-material-code').innerHTML = materialData;
      
      getMaterialCode.style.borderColor  = '#0C2427';
      getSubMaterialCode.style.borderColor = '#0C2427';
      getSubQuantity.style.borderColor = '#0C2427';

      addErrorMaterialField.innerHTML = '';
      addErrorSubMaterialField.innerHTML = '';
      addErrorQuantityField.innerHTML = '';       
    });    
  };

  this.closeAddForm = function() {
    addModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.Delete = function(item) {
    deletingID = item;
    deleteModule.style.display = 'flex';
    inactiveMask.style.display = 'block';
  };

  this.closeDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.applyDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    fetch(host+'api/billOfMaterial/delete.abs?id='+deletingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
    })
    .then(() => {
      this.FetchAll();
    });
  };

  this.Edit = function(item) {
    editModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    updatingID = item;

    editMaterialCode.style.borderColor = '#0C2427';
    editSubMaterialCode.style.borderColor = '#0C2427';
    editQuantity.style.borderColor = '#0C2427';

    Promise.all([
      fetch(host+'api/product/list.abs?'),
      fetch(host+'api/billOfMaterial/detail.abs?id='+updatingID)
    ])
    .then(result => Promise.all(result.map(v => v.json()))
    .then(function(data) {
      data[0].data.sort(function(a, b) {
        return (a.code.localeCompare(b.code));
      });
      console.log(data[0]);
      console.log(data[1].data);

      let materialData = '';

      data[0].data.forEach((material) => {
        materialData += '<option value="'+material.code+'">'+material.code+'</option>';
      });
      document.getElementById('edit-material-code').innerHTML = materialData;
      document.getElementById('edit-sub-material-code').innerHTML = materialData;

      editMaterialCode.value = data[1].data.materialCode;
      editSubMaterialCode.value = data[1].data.subMaterialCode;
      editQuantity.value = data[1].data.subQuantity;

      editErrorMaterialField.innerHTML = '';
      editErrorSubMaterialField.innerHTML = '';
      editErrorQuantityField.innerHTML = '';

    }));
  };

  this.closeEditForm = function() {
   editModule.style.display = 'none';
   inactiveMask.style.display = 'none';
  }

   this.applyEditForm = function() {
     if (this.Check('edit')) {

      let materialCode = editMaterialCode.value;
      let subMaterialCode = editSubMaterialCode.value;
      let subQuantity = editQuantity.value;

      fetch(host+'api/billOfMaterial/update.abs?id='+updatingID+'&materialCode='+materialCode+'&subMaterialCode='+subMaterialCode+'&subQuantity='+subQuantity)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
        })
        .then(() => {
          this.FetchAll();
          editModule.style.display = 'none';
          inactiveMask.style.display = 'none';
        });       
     }
   };

//   this.Sort = function(param) {
//     if (param === 'name') {
//       if (ascendingName) {
//         this.products.sort(compareDescendingName);
//         ascendingName = false;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingName);
//         ascendingName = true;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9650;"
//       }
//     };

//     if (param === 'price') {
//       if (ascendingPrice) {
//         this.products.sort(compareDescendingPrice);
//         ascendingPrice = false;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingPrice);
//         ascendingPrice = true;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9650;"
//       }
//     }

//     this.FetchAll();

//     function compareAscendingPrice(x, y) {
//       return x.price - y.price;
//     }
//     function compareDescendingPrice(x, y) {
//       return y.price - x.price;
//     }

//     function compareAscendingName(x, y) {
//       if (x.name > y.name) return 1;
//       if (x.name < y.name) return -1;
//     }
//     function compareDescendingName(x, y) {
//       if (x.name > y.name) return -1;
//       if (x.name < y.name) return 1;
//     }
//   };

//   // сортировка элементов таблицы по параметру name при загрузке страницы
//   this.Sort('name');

//   this.Search = function() {
//     let searchValue = document.getElementById('search-form').value.toLowerCase().trim();
//     let searchArray = this.products;
//     let searchCompleted = [];

//     for (i = 0; i < searchArray.length; i++) {
//       let currentStr = searchArray[i].name.slice(0, searchValue.length).toLowerCase();
//       if (searchValue === currentStr) {
//         searchCompleted.push(searchArray[i]);
//       }
//     }

//     this.products = searchCompleted;
//     this.FetchAll();
//     document.getElementById('search-form').value = '';
//   };

//   /*
//    * Проверяет корректность заполнения формы
//    *
//    * @param {get} - проверка формы добавления нового элемента
//    * @param {edit} - проверка формы редактирования элемента
//    * return {true/false} - корректно/некорректно заполнена форма
//    */
  this.Check = function(param) {
    let checker = true;

    if (param === 'get') {
      if (getMaterialCode.value.trim() === '') {
        getMaterialCode.style.borderColor = '#F2223C';
        addErrorMaterialField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getSubMaterialCode.value.trim() === '') {
        getSubMaterialCode.style.borderColor = '#F2223C';
        addErrorSubMaterialField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getSubMaterialCode.value === getMaterialCode.value) {
        getSubMaterialCode.style.borderColor = '#F2223C';
        addErrorSubMaterialField.innerHTML = 'These can not be the same value.';
        getMaterialCode.style.borderColor = '#F2223C';
        addErrorMaterialField.innerHTML = 'These can not be the same value.';
        checker = false;
      }
      if (getSubQuantity.value.trim() === '') {
        getSubQuantity.style.borderColor = '#F2223C';
        addErrorQuantityField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }

    if (param === 'edit') {
      if (editMaterialCode.value.trim() === '') {
        editMaterialCode.style.borderColor = '#F2223C';
        editErrorMaterialField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (editSubMaterialCode.value.trim() === '') {
        editSubMaterialCode.style.borderColor = '#F2223C';
        editErrorSubMaterialField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (editSubMaterialCode.value === editMaterialCode.value) {
        editSubMaterialCode.style.borderColor = '#F2223C';
        editErrorSubMaterialField.innerHTML = 'These can not be the same value.';
        editMaterialCode.style.borderColor = '#F2223C';
        editErrorMaterialField.innerHTML = 'These can not be the same value.';
        checker = false;
      }
      if (editQuantity.value.trim() === '') {
        editQuantity.style.borderColor = '#F2223C';
        editErrorQuantityField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }
  };

//   // подсвечивание некорректных значений после потери фокуса

//   getName.onblur = function() {
//     if (getName.value.trim() === '') {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (getName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                     ' exceed 15 characters.';
//     }
//   }
//   getName.onfocus = function() {
//       getName.style.borderColor = '#0C2427';
//       addErrorNameField.innerHTML = '';
//   };

//   getCount.onblur = function() {
//     if (getCount.value.trim() === '') {
//       getCount.style.borderColor = '#F2223C';
//       addErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getCount.onfocus = function() {
//       getCount.style.borderColor = '#0C2427';
//       addErrorCountField.innerHTML = '';
//   };

//   getPrice.onblur = function() {
//     if (getPrice.value.trim() === '') {
//       getPrice.style.borderColor = '#F2223C';
//       addErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getPrice.onfocus = function() {
//       getPrice.style.borderColor = '#0C2427';
//       addErrorPriceField.innerHTML = '';
//   };

//   editName.onblur = function() {
//     if (editName.value.trim() === '') {
//       editName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (editName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                      ' exceed 15 characters.';
//     }
//   }
//   editName.onfocus = function() {
//       editName.style.borderColor = '#0C2427';
//       editErrorNameField.innerHTML = '';
//   };

//   editCount.onblur = function() {
//     if (editCount.value.trim() === '') {
//       editCount.style.borderColor = '#F2223C';
//       editErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editCount.onfocus = function() {
//       editCount.style.borderColor = '#0C2427';
//       editErrorCountField.innerHTML = '';
//   };

//   editPrice.onblur = function() {
//     if (editPrice.value.trim() === '') {
//       editPrice.style.borderColor = '#F2223C';
//       editErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editPrice.onfocus = function() {
//       editPrice.style.borderColor = '#0C2427';
//       editErrorPriceField.innerHTML = '';
//   };

};
app.FetchAll();

